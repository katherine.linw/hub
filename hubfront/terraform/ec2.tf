resource "aws_security_group" "ec2_sg" {
  vpc_id = aws_vpc.main.id  
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
    egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_instance" "ec2" {
    ami = "ami-09c8d5d747253fb7a"
    instance_type="t2.micro"
    key_name="OpenSSHKey"
    subnet_id = aws_subnet.main_public_subnet.id
    vpc_security_group_ids = [aws_security_group.ec2_sg.id]
    associate_public_ip_address = true
    user_data = "${file("userdata.sh")}"
}

output "ec2_global_ips" {
  value = aws_instance.ec2.public_ip
}

