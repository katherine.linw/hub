resource "aws_vpc" "main" {
    cidr_block = "172.0.0.0/16"
}

/*
resource "aws_vpc" "vice"{
    cidr_block = "172.1.0.0/16"
}
*/

resource "aws_subnet" "main_public_subnet"{
    vpc_id = aws_vpc.main.id
    cidr_block = "172.0.1.0/24"
}

resource "aws_subnet" "main_private_subnet"{
    vpc_id = aws_vpc.main.id
    count = length(var.private_subnet_cidrs)
    cidr_block = element(var.private_subnet_cidrs,count.index)


    tags = {
        Name = "Private Subnet ${count.index + 1}"
    }
}

resource "aws_internet_gateway" "igw" {
    vpc_id = aws_vpc.main.id
}

resource "aws_route_table" "rt" {
    vpc_id = aws_vpc.main.id
    route {
        cidr_block ="0.0.0.0/0"
        gateway_id = aws_internet_gateway.igw.id
    }
}

resource "aws_route_table_association" "a" {
    subnet_id = aws_subnet.main_public_subnet.id
    route_table_id = aws_route_table.rt.id 
}