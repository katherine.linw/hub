variable "private_subnet_cidrs" {
  type = list(string)
  default = [ "172.0.3.0/24","172.0.4.0/24"]
}